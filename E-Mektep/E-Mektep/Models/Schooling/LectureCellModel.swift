//
//  LectureCellModel.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 24.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import SwiftyJSON

class LectureCellModel {
    
    init(json: JSON) {
        self.DersKodu = json["DersKodu"].stringValue;
        self.DersAdi = json["DersAdi"].stringValue;
        self.DersKredisi = json["Kredi"].intValue;
        self.UyeID = json["UyeID"].intValue;
        self.TeorikSaat = json["TeorikSaat"].intValue;
        self.UygulamaSaat = json["UygulamaSaat"].intValue;
        self.Kredi = json["Kredi"].intValue;
        self.OgretimUyesi = json["OgretimUyesi"].stringValue;
        self.DersID = json["DersID"].intValue;
        self.DonemID = json["DonemID"].intValue;
    }

    var DersKodu: String!
    var DersAdi: String!
    var DersKredisi: Int!
    var UyeID: Int!
    var TeorikSaat: Int!
    var UygulamaSaat: Int!
    var Kredi: Int!
    var OgretimUyesi: String!
    var DersID: Int!
    var DonemID: Int!
    
}
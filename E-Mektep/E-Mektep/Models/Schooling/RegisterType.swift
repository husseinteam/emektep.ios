//
//  LectureCellModel.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 24.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation

enum RegisterType : Int {
    case OSS = 1
    case YatayGecis = 2
    case EkKontenjan = 3
    func description() -> (id: Int, text: String) {
        switch self {
        case .OSS:
            return (id: RegisterType.OSS.rawValue, text: "ÖSS İle Gelen")
        case .YatayGecis:
            return (id: RegisterType.YatayGecis.rawValue, text: "Yatay Geçiş")
        case .EkKontenjan:
            return (id: RegisterType.EkKontenjan.rawValue, text: "Ek Kontenjan İle Gelen")
        }
    }
    static func getRange() -> [(id: Int, text: String)] {
        return [
            RegisterType.OSS.description(),
            RegisterType.YatayGecis.description(),
            RegisterType.EkKontenjan.description()
        ]
    }
    
}
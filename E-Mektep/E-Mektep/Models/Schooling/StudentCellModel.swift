//
//  LectureCellModel.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 24.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import SwiftyJSON

class StudentCellModel {
    
    init(json: JSON) {
        self.OgrenciNo = json["OgrenciNo"].stringValue;
        self.AdiSoyadi = json["OgrenciAdiSoyadi"].stringValue;
        self.UyeID = json["UyeID"].intValue;
        self.OgretimUyesiUyeID = json["OgretimUyesiUyeID"].intValue;
        self.KayitYili = json["OgrenciKayitYili"].intValue;
        self.KayitTipi = RegisterType(rawValue: json["OgrenciKayitTipi"].intValue);
    }

    var OgrenciNo: String!
    var AdiSoyadi: String!
    var UyeID: Int!
    var OgretimUyesiUyeID: Int!
    var KayitYili: Int!
    var KayitTipi: RegisterType!
    
}
//
//  ExamModel.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 10.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import SwiftyJSON

class ExamModel {
    
    init(json: JSON) {
        self.id = json["ID"].intValue
        self.title = json["Title"].stringValue
        self.examDate = json["ExamDate"].stringValue
        self.studentID = json["StudentID"].intValue
        self.lectureID = json["LectureID"].intValue
    }
    
    var id: Int!
    var title: String!
    var examDate: String!
    var studentID: Int!
    var lectureID: Int!
    
}

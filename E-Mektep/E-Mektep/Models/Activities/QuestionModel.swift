//
//  QueestionModel.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 10.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import SwiftyJSON
import ResearchKit

class QuestionModel {

    init(json: JSON) {
        self.id = json["ID"].intValue
        self.identifier = json["Identifier"].stringValue
        self.title = json["Title"].stringValue
        self.questionType = QuestionType(rawValue: json["QuestionType"].intValue)
        self.examID = json["ExamID"].intValue
    }
    
    var id: Int!
    var identifier: String!
    var title: String!
    var questionType: QuestionType!
    var examID: Int!
    var testChoices: [TestChoiceModel]?
    
    func toQuestionStep() -> ORKQuestionStep {
        // Quest question using text choice
        let questQuestionStepTitle = self.title
        let textChoices = self.testChoices?.map { ORKTextChoice(text: $0.text, value: $0.value) }
        let questAnswerFormat: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormatWithStyle(.SingleChoice, textChoices: textChoices!)
        let questQuestionStep = ORKQuestionStep(identifier: self.identifier, title: questQuestionStepTitle, answer: questAnswerFormat)
        return questQuestionStep
    }
    
}
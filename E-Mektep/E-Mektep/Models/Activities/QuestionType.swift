//
//  QuestionType.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 10.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation

enum QuestionType : Int {
    case Test = 0
    case Category = 1
    case Name = 2
    case Image = 3
    case Date = 4
    case Boolean = 5
    case Continuous = 6
}
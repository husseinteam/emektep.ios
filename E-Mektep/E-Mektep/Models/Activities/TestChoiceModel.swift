//
//  File.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 10.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import SwiftyJSON

class TestChoiceModel {
    
    init(json: JSON) {
        self.id = json["ID"].intValue
        self.text = json["Text"].stringValue
        self.correct = json["Correct"].boolValue
        self.value = json["Value"].intValue
        self.questionID = json["QuestionID"].intValue
    }
    
    var id: Int!
    var text: String!
    var correct: Bool!
    var value: Int!
    var questionID: Int!

}
//
//  ClassModel.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 1.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import SwiftyJSON

class ClassModel {
    
    init(json: JSON) {
        self.beginHour = json["BaslangicSaati"].stringValue
        self.endHour = json["BitisSaati"].stringValue
        self.nameOfClass = json["DersAdi"].stringValue
        self.nameOfLecturer = json["DersiVeren"].stringValue
        self.nameOfClassRoom = json["DerslikAdi"].stringValue
        self.dayOfWeek = DayOfWeek(rawValue: json["DersGunu"].intValue)
        self.clockColor = ShapeColor(rawValue: json["DersGunu"].intValue)
    }
    
    init(begin: String, end: String, nameOfClass: String, lecturer: String, classRoom: String, dayOfWeek: DayOfWeek, clockColor: ShapeColor) {
        self.beginHour = begin
        self.endHour = end
        self.nameOfClass = nameOfClass
        self.nameOfLecturer = lecturer
        self.nameOfClassRoom = classRoom
        self.dayOfWeek = dayOfWeek
        self.clockColor = clockColor
    }
    
    var beginHour: String!
    var endHour: String!
    var nameOfClass: String!
    var nameOfLecturer: String!
    var nameOfClassRoom: String!
    var dayOfWeek: DayOfWeek!
    var clockColor: ShapeColor!
    
    static var MockedData = [
        ClassModel(begin: "08:00", end: "09:00", nameOfClass: "Türk Dili", lecturer: "Öğr. Gör. Dilek Sönmez", classRoom: "Derslik 1", dayOfWeek: .Pazartesi, clockColor: .Red),
        ClassModel(begin: "09:00", end: "10:00", nameOfClass: "Matematik", lecturer: "Öğr. Gör. Dilek Sönmez", classRoom: "Derslik 2", dayOfWeek: .Pazartesi, clockColor: .Yellow),
        ClassModel(begin: "10:00", end: "11:00", nameOfClass: "Ataput İlkeleri ve İnkilap Tarihi", lecturer: "Öğr. Gör. Mahmut Sarigül", classRoom: "Derslik -1", dayOfWeek: .Pazartesi, clockColor: .Brown),
        ClassModel(begin: "11:00", end: "12:00", nameOfClass: "Kimya", lecturer: "Öğr. Gör. Deniz Simyacı", classRoom: "Derslik 3", dayOfWeek: .Pazartesi, clockColor: .Green),
        ClassModel(begin: "13:00", end: "14:00", nameOfClass: "Fizik", lecturer: "Öğr. Gör. Dilek Sönmez", classRoom: "Derslik 1", dayOfWeek: .Pazartesi, clockColor: .Orange),
        ClassModel(begin: "08:00", end: "09:00", nameOfClass: "Din Kültürü ve Ahlak Bilgisi", lecturer: "Öğr. Gör. Dilek Sönmez", classRoom: "Derslik 1", dayOfWeek: .Sali, clockColor: .Red),
        ClassModel(begin: "09:00", end: "10:00", nameOfClass: "Ehli Sünnet Kelam İlmi", lecturer: "Öğr. Gör. Dilek Sönmez", classRoom: "Derslik 2", dayOfWeek: .Sali, clockColor: .Yellow),
        ClassModel(begin: "10:00", end: "11:00", nameOfClass: "Ehli Bidat Fırkalar", lecturer: "Öğr. Gör. Mahmut Sarigül", classRoom: "Derslik -1", dayOfWeek: .Sali, clockColor: .Purple),
        ClassModel(begin: "11:00", end: "12:00", nameOfClass: "Riyaziye", lecturer: "Öğr. Gör. Deniz Simyacı", classRoom: "Derslik 3", dayOfWeek: .Sali, clockColor: .Blue),
        ClassModel(begin: "13:00", end: "14:00", nameOfClass: "Cebir", lecturer: "Öğr. Gör. Dilek Sönmez", classRoom: "Derslik 1", dayOfWeek: .Sali, clockColor: .Green)
    ]
    
}
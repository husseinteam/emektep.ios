//
//  DayOfWeek.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 1.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation

enum DayOfWeek : Int {
    
    case Cuma = 0
    case Cumartesi = 1
    case Pazar = 2
    case Pazartesi = 3
    case Sali = 4
    case Carsamba = 5
    case Persembe = 6
    case HaftaIci = 7

    func description() -> (id: Int, text: String) {
        switch self {
        case .Cuma:
            return (id: DayOfWeek.Cuma.rawValue, text: "Cuma")
        case .Cumartesi:
            return (id: DayOfWeek.Cumartesi.rawValue, text: "Cumartesi")
        case .Pazar:
            return (id: DayOfWeek.Pazar.rawValue, text: "Pazar")
        case .Pazartesi:
            return (id: DayOfWeek.Pazartesi.rawValue, text: "Pazartesi")
        case .Sali:
            return (id: DayOfWeek.Sali.rawValue, text: "Salı")
        case .Carsamba:
            return (id: DayOfWeek.Carsamba.rawValue, text: "Çarşamba")
        case .Persembe:
            return (id: DayOfWeek.Persembe.rawValue, text: "Persembe")
        case .HaftaIci:
            return (id: DayOfWeek.HaftaIci.rawValue, text: "Hafta İçi")
        }
    }
    static func getRange() -> [(id: Int, text: String)] {
        return [
            DayOfWeek.Cuma.description(),
            DayOfWeek.Cumartesi.description(),
            DayOfWeek.Pazar.description(),
            DayOfWeek.Pazartesi.description(),
            DayOfWeek.Sali.description(),
            DayOfWeek.Carsamba.description(),
            DayOfWeek.Persembe.description(),
            DayOfWeek.HaftaIci.description()
        ]
    }
}
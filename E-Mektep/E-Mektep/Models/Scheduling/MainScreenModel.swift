//
//  MainScreenModel.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 4.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import UIKit

class MainScreenModel {
    
    init(color: ShapeColor, title: String, width: CGFloat, height: CGFloat) {
        self.cellColor = color
        self.title = title
        
        self.size = CGSize(width: width, height: height)
    }
    var cellColor: ShapeColor!
    var title: String!
    var image: UIImage? = nil
    var size: CGSize!
    var callback: ((model: MainScreenModel) -> Void)?
    
    func setImageName(name: String) -> MainScreenModel {
        self.image = UIImage(named: name)
        return self
    }
    
    func setCallback(callback: (model: MainScreenModel) -> Void) -> MainScreenModel {
        
        self.callback = callback
        return self
        
    }
    
}
//
//  LectureCell.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 24.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit

class LectureCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBOutlet weak var dersKoduLabel: UILabel!
    @IBOutlet weak var dersAdiLabel: UILabel!
    
    var model: LectureCellModel! {
        didSet {
            self.dersKoduLabel.text = self.model.DersKodu;
            self.dersAdiLabel.text = self.model.DersAdi;
        }
    }


}

//
//  ClassTableCell.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 1.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit

class ClassTableCell: UITableViewCell {

    @IBOutlet weak var baslangicSaatiLabel: UILabel!
    @IBOutlet weak var bitisSaatiLabel: UILabel!
    @IBOutlet weak var dersAdiLabel: UILabel!
    @IBOutlet weak var dersiVerenLabel: UILabel!
    @IBOutlet weak var derslikAdiLabel: UILabel!
    
    @IBOutlet weak var clockCircle: ScheduleDateView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var model: ClassModel! {
        didSet {
            self.baslangicSaatiLabel?.text = self.model.beginHour
            self.bitisSaatiLabel?.text = self.model.endHour
            self.dersAdiLabel?.text = self.model.nameOfClass
            self.dersiVerenLabel?.text = self.model.nameOfLecturer
            self.derslikAdiLabel?.text = self.model.nameOfClassRoom
            self.clockCircle?.fillColor = self.model.clockColor.description()
            self.clockCircle?.setUp(self.model)
        }
    }

}

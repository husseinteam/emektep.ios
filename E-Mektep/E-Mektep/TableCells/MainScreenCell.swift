//
//  MainScreenCell.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 2.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit

class MainScreenCell: UICollectionViewCell {

    @IBOutlet weak var rectView: RectangleView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func updateUI() {
        if let m = self.model {
            if let _ = self.viewWithTag(1453) {
                
            } else {
                self.frame.size = m.size
                self.rectView.frame.size = m.size
                self.rectView.fillColor = m.cellColor.description()
                let title = UILabel(frame: self.rectView.frame)
                title.text = m.title
                title.textAlignment = .Center
                title.lineBreakMode = .ByWordWrapping
                title.numberOfLines = 0
                title.textColor = m.cellColor.inverted()
                title.font = UIFont(name: "OpenSans", size: 13.0)!
                title.tag = 1453
                self.addSubview(title)
                title.sizeToFit()
                title.putOnCenterOfSuperview(self)
                if let logo = m.image {
                    let img = UIImageView(image: logo)
                    self.addSubview(img)
                    img.layer.opacity = 0.3
                    img.putOnCenterOfSuperview(self, scaled: 0.85)
                }
            }
        }
    }
    
    var model: MainScreenModel? {
        didSet {
            self.updateUI()
        }
    }
    
}

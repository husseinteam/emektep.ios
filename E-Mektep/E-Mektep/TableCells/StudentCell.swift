//
//  StudentCell.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 25.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit

class StudentCell: UITableViewCell {

    @IBOutlet weak var ogrenciNoLabel: UILabel!
    @IBOutlet weak var adiSoyadiLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var model: StudentCellModel! {
        didSet {
            self.ogrenciNoLabel.text = String(self.model.OgrenciNo);
            self.adiSoyadiLabel.text = "\(self.model.AdiSoyadi)";
        }
    }
    
}

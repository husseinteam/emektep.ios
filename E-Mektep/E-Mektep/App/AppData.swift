//
//  AppData.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 24.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import UIKit

class AppData {
    static var debugModeOn = true;
    class URL {
        static func toApi(route: String) -> String {
            return AppData.debugModeOn ?
                "http://192.168.1.112:114/api/\(route)" :
                "http://app.lampiclobe.com/api/\(route)";
        }
    }
    static var uiSize = UIScreen.mainScreen().bounds.size
    class CurrentDate {
        static func dayOfWeek() -> DayOfWeek {
            return DayOfWeek(rawValue: (CurrentDate.weekday() - 5) %% 7)!
        }
        static func year() -> Int {
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Year, fromDate: date)
            return components.year
        }
        static func populateSince(past: Int) -> [Int] {
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Year, fromDate: date)
            return [Int](past...components.year)
        }
        static func month() -> Int {
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Month, fromDate: date)
            return components.month
        }
        static func day() -> Int {
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Day, fromDate: date)
            return components.day
        }
        static func weekday() -> Int {
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components(.Weekday, fromDate: date)
            return components.weekday
        }
    }
    class UserDefaults {
        static var TCKN: String? {
            get {
                let ud = NSUserDefaults.standardUserDefaults()
                if let tckn = ud.stringForKey("TCKN") {
                    return tckn;
                } else {
                    return nil;
                }
            }
            set {
                let ud = NSUserDefaults.standardUserDefaults();
                ud.setObject(newValue, forKey: "TCKN")
            }
        }
        static var Password: String? {
            get {
                let ud = NSUserDefaults.standardUserDefaults()
                if let password = ud.stringForKey("Password") {
                    return password;
                } else {
                    return nil;
                }
            }
            set {
                let ud = NSUserDefaults.standardUserDefaults();
                ud.setObject(newValue, forKey: "Password")
            }
        }
    }
    class Session {
        static var Token: String?  {
            get {
                let ud = NSUserDefaults.standardUserDefaults()
                if let token = ud.stringForKey("UserAccessToken") {
                    return token;
                } else {
                    return nil;
                }
            }
            set {
                let ud = NSUserDefaults.standardUserDefaults();
                ud.setObject(newValue, forKey: "UserAccessToken")
            }
        }
    }
}
//
//  AnimeField.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 23.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit
import Canvas
import EasyTipView

@IBDesignable
class IndentedField: UITextField {

    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectOffset(bounds, bounds.height * 0.75, 0.0)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return textRectForBounds(bounds);
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
}





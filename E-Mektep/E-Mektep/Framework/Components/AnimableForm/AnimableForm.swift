//
//  AnimableForm.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 23.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit
import Canvas
import Foundation
import Alamofire
import SwiftyJSON
import PKHUD
import ReachabilitySwift

@IBDesignable
class AnimableForm: CSAnimationView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(CGColor: color);
            } else {
                return UIColor.clearColor()
            }
        }
        set {
            layer.borderColor = newValue!.CGColor;
            layer.borderWidth = 1;
        }
    }
    
    @IBInspectable var animationDuration: Double {
        get {
            return self.duration;
        }
        set {
            self.duration = newValue;
        }
    }
    
    @IBInspectable var animationDelay: Double {
        get {
            return self.delay;
        }
        set {
            self.delay = newValue;
        }
    }
    
    @IBInspectable var animationType: String {
        get {
            return self.type;
        }
        set {
            self.type = newValue;
        }
    }
    
    private var fields: [AnimeField]!;
    
    private func getFields() -> [AnimeField]{
        
        var animes = [AnimeField]();
        self.loopThrough(&animes);
        return animes;
        
    }
    
    private var baseViewController: PublicViewController? {
        get {
            return self.parentViewController as? PublicViewController
        }
    }
    func submit(route: String, postback: (data: JSON, token: String) -> Void) {
        
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return
        }
        reachability.whenReachable = { reachability in
            dispatch_async(dispatch_get_main_queue()) {
                var result = true;
                let fields = self.getFields();
                for anime in fields {
                    if !anime.validate() {
                        result = false;
                    }
                }
                if result {
                    HUD.show(.LabeledProgress(title: "E-Mektep", subtitle: "Yükleniyor.."));
                    var parameters = [String:String]();
                    if let token = AppData.Session.Token {
                        parameters["token"] = token;
                    }
                    for anime in fields {
                        parameters[anime.FormField!] = anime.value;
                    }
                    Alamofire.request(.POST, AppData.URL.toApi(route),
                        parameters: parameters, encoding: .JSON)
                        .responseJSON(completionHandler: { response in
                            HUD.hide(animated: false)
                            if let err = response.result.error {
                                if err is NSURLError {
                                    self.baseViewController?.showAlert(AlertMode.Error, title: "E-Mektep", subTitle: "Servise Erişilemiyor")
                                } else {
                                    err.log()
                                }
                            } else if let jsonString = response.result.value {
                                let json = JSON(jsonString)
                                if json["done"].boolValue {
                                    postback(data: json["data"], token: json["token"].stringValue)
                                } else {
                                    self.baseViewController?.showAlert(.Error, title: "E-Mektep",
                                        subTitle: json["messages"][0].stringValue.nevertheless("Kritik Servis Hatası") { elseWhere in Error.errorWithCode(-1, failureReason: elseWhere).log() } )
                                }
                            }
                        })
                }
            }
        }
        reachability.whenUnreachable = { reachability in
            dispatch_async(dispatch_get_main_queue()) {
                HUD.show(.LabeledError(title: "E-Mektep", subtitle: "Internet Bağlantınızı Kontrol Ediniz"));
                HUD.hide(afterDelay: 1.7, completion: { (fine) in
                    exit(0)
                })
            }
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.backgroundColor = UIColor(white: 1, alpha: self.alpha)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        self.backgroundColor = UIColor(white: 1, alpha: self.alpha)
    }
    
}

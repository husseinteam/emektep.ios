//
//  AnimeField.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 23.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import UIKit
import Canvas
import EasyTipView
import AKPickerView_Swift

@IBDesignable
class AnimeField: CSAnimationView, EasyTipViewDelegate {
    
    @IBOutlet weak var FieldIconImageView: UIImageView!
    @IBOutlet private weak var TextField: UITextField!
    
    @IBInspectable var placeholder: String {
        get {
            return TextField.placeholder ?? "";
        }
        set {
            TextField.placeholder = newValue;
        }
    }
    
    @IBInspectable var Icon: UIImage? {
        get {
            return FieldIconImageView.image;
        }
        set {
            FieldIconImageView.image = newValue;
            FieldIconImageView.setNeedsDisplay();
        }
    }
    
    @IBInspectable var InvalidatedMessage: String?;
    @IBInspectable var FormField: String?;
    
    var value: String! {
        get {
            return self.pickedID > -1 ? self.pickedID?.astr() : self.TextField.text
        }
        set {
            self.TextField.text = newValue
        }
    }
    
    private var max: Int! = 800;
    private var contentType: AFContentType = .Text(max: 800)
    private var pickerRange: [(id: Int, text: String)]? = nil {
        didSet {
            if let range = self.pickerRange {
                self.pickerView = ListViewPicker()
                self.pickerView?.setModel([range]).setPickerSelected({ (row, component, model) in
                    self.pickedItem = model[component][row]
                })
            }
        }
    }
    var pickedID: Int? = -1
    private var pickedItem: (id: Int, text: String)? = nil {
        didSet {
            if let (id, value) = self.pickedItem {
                self.TextField.text = value
                self.pickedID = id
            }
        }
    }
    private var validators = Array<(field: AnimeField) -> Bool>()
    
    internal func validate() -> Bool {
        let result = self.validators.all { (validator) -> Bool in
            return validator(field: self);
        }
        if !result {
            self.TextField.background = UIImage(named: "stroke-red-input");
            self.shake()
            if let message = self.InvalidatedMessage {
                self.setTooltip(message);
                self.tipView?.show(forView: self);
                1.7 ~ { self.tipView?.dismiss() }
            }
        } else {
            self.TextField.background = UIImage(named: "stroke-input");
            self.tipView?.dismiss();
        }
        return result;
    }
    
    internal func setValidator(validator: (field: AnimeField) -> Bool) -> AnimeField {
        
        self.validators.append(validator);
        return self;
        
    }
    
    internal func setContentType(type: AFContentType) -> AnimeField {
        
        self.contentType = type;
        switch type {
        case .Password(let length):
            self.TextField.secureTextEntry = true;
            self.max = length;
        case .Text(let length):
            self.max = length;
        case .TCKN(let length):
            self.max = length;
        case .Number:
            self.max = Int.max;
        case .Pickered(let range):
            self.pickerRange = range
        }
        return self;
        
    }
    
    private var tipView: EasyTipView?;
    
    private func setTooltip(message: String, bgcolor: UIColor? = nil) {
        
        var preferences = EasyTipView.Preferences();
        preferences.drawing.font = UIFont(name: "Helvetica Neue", size: 13)!;
        preferences.drawing.foregroundColor = UIColor.whiteColor();
        preferences.drawing.backgroundColor = bgcolor ?? UIColor(red: 219/255, green: 40/255, blue: 40/255, alpha: 1.0);
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.Bottom;
        self.tipView?.dismiss();
        self.tipView = EasyTipView(text: message, preferences: preferences)

    }
    
    private func shake() {
        self.duration = 0.5;
        self.delay = 0.3
        self.type = CSAnimationTypeShake;
        self.translatesAutoresizingMaskIntoConstraints = true;
        self.startCanvasAnimation();
        self.translatesAutoresizingMaskIntoConstraints = false;
        
    }
    private var pickerView: ListViewPicker? = nil
    private func totalSetup() {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "AnimeField", bundle: bundle)
        if let view =  nib.instantiateWithOwner(self, options: nil).first as? UIView {
            view.frame = self.bounds;
            // Make the view stretch with containing view
            view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
            addSubview(view);
        }
        
        self.TextField.delegate = self;
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.textFieldTapped(_:)))
        self.userInteractionEnabled = true
        self.addGestureRecognizer(tapGesture)

        self.validators.append({ field in field.TextField.text?.characters.count <= field.max && field.TextField.text?.characters.count > 0 })
        
    }
    
    func textFieldTapped(sender: UITapGestureRecognizer) {
        self.pickerView?.bringOut(self.TextField.placeholder ?? "", markerRect: self.TextField.frame)
    }
    
    func easyTipViewDidDismiss(tipView: EasyTipView) {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        totalSetup();
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        totalSetup();
    }

}

extension AnimeField : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if self.pickerRange == nil {
            self.setTooltip(self.TextField.placeholder!, bgcolor: UIColor(red: 0.0, green: 130/255, blue: 124/255, alpha: 0.7));
            self.tipView?.show(forView: self);
            1.7 ~ { self.tipView?.dismiss() }
        }
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.tipView?.dismiss();
    }

    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if self.pickerRange != nil {
            self.pickerView?.bringOut(self.TextField.placeholder ?? "", markerRect: self.TextField.frame)
            return false
        } else {
            return true
        }
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        self.pickerView?.bringOut(self.TextField.placeholder ?? "", markerRect: self.TextField.frame)
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.characters.count >= self.max {
            return string == "";
        }
        switch self.contentType {
        case .Text:
            return true;
        case .TCKN:
            return string.isDigit() || string == "";
        default:
            return true;
        }
    }
    
}





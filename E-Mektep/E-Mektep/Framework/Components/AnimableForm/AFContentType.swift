//
//  AFContentType.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 24.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation

enum AFContentType {
    case Text(max: Int)
    case TCKN(max: Int)
    case Password(max: Int)
    case Number
    case Pickered(range: [(id: Int, text: String)])
}
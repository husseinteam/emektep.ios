//
//  Speaker.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 27.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit
import Canvas

@IBDesignable
class SpeakerView: UIView {
    
    private var spokenView: UIView!
    private var shadowView: UIView!
    private var shadowGesture: UITapGestureRecognizer!
    func bringOut(withTitle: String, markerRect: CGRect) {
        
        let nib = UINib(nibName: String(self.dynamicType), bundle: nil)
        if var view = nib.instantiateWithOwner(self, options: nil).first as? UIView {
            let cover = UIScreen.mainScreen().bounds
            self.shadowView = UIView(frame: cover)
            shadowView.backgroundColor = UIColor(hash: "#555555")
            shadowView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
            shadowView.translatesAutoresizingMaskIntoConstraints = true
            
            shadowGesture = UITapGestureRecognizer(target: self, action: #selector(self.shadowTapped(_:)))
            shadowView.userInteractionEnabled = true
            shadowView.addGestureRecognizer(shadowGesture)
            
            UIApplication.sharedApplication().keyWindow?.addSubview(shadowView)
            
            view.alpha = 0.0
            view.layer.cornerRadius = 5.0
            view.layer.masksToBounds = true
            
            view.addSubview(self)
            UIApplication.sharedApplication().keyWindow?
                .insertSubview(view, aboveSubview: self.shadowView)
            view.putOnCenter(markerRect.width * 1.3 / cover.width, scaleY: 0.7)
            
            let flexibleItem = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
            let titleItem = UIBarButtonItem(title: withTitle, style: .Done, target: nil, action: nil)
            titleItem.width = view.frame.width * 0.5
            titleItem.tintColor = UIColor(hash: "#008200")
            let toolrow = UIToolbar(frame: CGRectMake(0, 0, view.frame.width, 44))
            toolrow.setBackgroundImage(UIColor(hash: "#FFFFFF").convertToImage(toolrow.frame.size), forToolbarPosition: .Any, barMetrics: .Default)
            toolrow.setItems([flexibleItem, titleItem, flexibleItem], animated: false)
            self.insertSubview(toolrow, aboveSubview: view)
            
            shadowView.fade(to: 0.7, forDuration: 0.5)
            view.fade(to: 1.0, forDuration: 0.5) { d in
                self.spokenView = view
                self.displayedCallback?(spoken: self)
            }
          
            
        }
        
    }
    
    func shadowTapped(sender: UITapGestureRecognizer) {
        self.putAside()
    }
    
    func putAside() {
        self.shadowView.fadeOut() { done in
            self.shadowView.removeFromSuperview()
        }
        self.spokenView.fadeOut() { done in
            self.spokenView.removeFromSuperview()
        }
        
    }
    
    private var displayedCallback: ((spoken: SpeakerView) -> Void)? = nil
        
    func when(displayed: (spoken: SpeakerView) -> Void) -> SpeakerView {
        
        self.displayedCallback = displayed
        return self
        
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}

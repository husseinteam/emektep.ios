//
//  ColoredView.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 4.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import UIKit

class RectangleView: UIView {
    
    @IBInspectable var fillColor: UIColor! {
        didSet {
            self.setUp(self.fillColor)
        }
    }
    
    private func setUp(fillColor: UIColor, strokeColor: UIColor = UIColor.clearColor()) {
        
        let path = UIBezierPath(rect: self.bounds)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.CGPath
        
        //change the fill color
        shapeLayer.fillColor = fillColor.CGColor
        //you can change the stroke color
        shapeLayer.strokeColor = strokeColor.CGColor
        //you can change the line width
        shapeLayer.lineWidth = 1.0
        
        self.layer.addSublayer(shapeLayer)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}

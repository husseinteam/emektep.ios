//
//  ShapeColor.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 1.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import UIKit

enum ShapeColor : Int {
    case Red = 1
    case Green = 2
    case Blue = 3
    case Black = 4
    case DarkGray = 5
    case LightGray = 6
    case White = 7
    case Gray = 8
    case Cyan = 9
    case Yellow = 10
    case Magenta = 11
    case Orange = 12
    case Purple = 13
    case Brown = 14
    case Clear = 15
    func description() -> UIColor {
        switch self {
        case .Red:
            return UIColor(hash: "#FF0F00")
        case .Green:
            return UIColor(hash: "#009200")
        case .Blue:
            return UIColor(hash: "#0000B1")
        case .Black:
            return UIColor(hash: "#000000")
        case .DarkGray:
            return UIColor(hash: "#333333")
        case .LightGray:
            return UIColor(hash: "#999999")
        case .White:
            return UIColor(hash: "#FFFFFF")
        case .Gray:
            return UIColor(hash: "#666666")
        case .Cyan:
            return UIColor(hash: "#99FF99")
        case .Yellow:
            return UIColor(hash: "#FFFF3B")
        case .Magenta:
            return UIColor(hash: "#FF0078#")
        case .Orange:
            return UIColor(hash: "#FF7500")
        case .Purple:
            return UIColor(hash: "#7F058D")
        case .Brown:
            return UIColor(hash: "#693C00")
        case .Clear:
            return UIColor.clearColor()
        }
    }
    func inverted() -> UIColor {
        switch self {
        case .Red:
            return ShapeColor.White.description()
        case .Green:
            return ShapeColor.White.description()
        case .Blue:
            return ShapeColor.Yellow.description()
        case .Black:
            return ShapeColor.White.description()
        case .DarkGray:
            return ShapeColor.White.description()
        case .LightGray:
            return ShapeColor.White.description()
        case .White:
            return ShapeColor.Black.description()
        case .Gray:
            return ShapeColor.White.description()
        case .Cyan:
            return ShapeColor.Black.description()
        case .Yellow:
            return ShapeColor.Black.description()
        case .Magenta:
            return ShapeColor.White.description()
        case .Orange:
            return ShapeColor.White.description()
        case .Purple:
            return ShapeColor.White.description()
        case .Brown:
            return ShapeColor.White.description()
        case .Clear:
            return ShapeColor.Black.description()
        }
    }
    
}
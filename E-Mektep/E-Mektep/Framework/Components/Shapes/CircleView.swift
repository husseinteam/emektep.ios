//
//  RoundedCircleView.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 1.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CircleView : UIView {
    
    @IBInspectable var fillColor: UIColor! {
        didSet {
            self.setUp(self.fillColor)
        }
    }
    
    private func setUp(fillColor: UIColor, strokeColor: UIColor = UIColor.clearColor()) {
    
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: self.center.x, y: self.center.y), radius: self.frame.width / 2, startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.CGPath
        
        //change the fill color
        shapeLayer.fillColor = fillColor.CGColor
        //you can change the stroke color
        shapeLayer.strokeColor = strokeColor.CGColor
        //you can change the line width
        shapeLayer.lineWidth = 1.0
        
        self.layer.addSublayer(shapeLayer)
    
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
//
//  ListViewPicker.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 27.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit

class ListViewPicker: SpeakerView {
    
    @IBOutlet weak var picker: UIPickerView! {
        didSet {
            self.picker.selectRow(self.model[0].count / 2, inComponent: self.model.count / 2, animated: false)
        }
    }

    private var model = [[(id: Int, text: String)]]()
    private var pickerSelected: ((row: Int, component: Int, model: [[(id: Int, text: String)]]) -> Void)!

    func setModel(model: [[(id: Int, text: String)]]) -> ListViewPicker {
        
        self.model = model
        
        return self
    
    }
    
    func setPickerSelected(callback: (row: Int, component: Int, model: [[(id: Int, text: String)]]) -> Void) -> ListViewPicker {
       
        self.pickerSelected = callback
        return self
        
    }

}

extension ListViewPicker : UIPickerViewDelegate {
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(self.model[component][row].text)
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.pickerSelected(row: row, component: component, model: self.model)
        self.putAside() 
    }
    
}


extension ListViewPicker : UIPickerViewDataSource {
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.model[component].count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return self.model.count
    }
    
}




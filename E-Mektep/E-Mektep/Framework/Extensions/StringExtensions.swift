//
//  AnimationExtensions.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 23.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import UIKit
import Canvas

extension String {
    
    func isDigit() -> Bool {
        
        var letterCount = 0
        var digitCount = 0
        
        Characterize(&letterCount, digitCount: &digitCount)
        return letterCount == 0 && digitCount > 0;
        
    }
    
    func Characterize(inout letterCount: Int, inout digitCount: Int) {
        let letters = NSCharacterSet.letterCharacterSet()
        let digits = NSCharacterSet.decimalDigitCharacterSet()
        
        for uni in self.unicodeScalars {
            if letters.longCharacterIsMember(uni.value) {
                letterCount += 1;
            } else if digits.longCharacterIsMember(uni.value) {
                digitCount += 1;
            }
        }
    }
    
    subscript(begin: Int, end: Int) -> String {
        return self.substringWithRange(self.startIndex.advancedBy(begin)..<self.startIndex.advancedBy(end))
    }
    
    func allChars(body: (Character) -> Bool) -> Bool {
        
        for el in self.characters {
            if !body(el) {
                return false;
            }
        }
        return true;
        
    }
    
}

extension String {
    
    func nevertheless(elseWhere: String, callback: ((ew: String) -> Void)? = nil) -> String {
        if self.isEmpty {
            callback?(ew: elseWhere)
            return elseWhere
        } else {
            return self
        }
    }
    
}

extension Int {
    
    func astr() -> String {
        return String(self);
    }
    
}
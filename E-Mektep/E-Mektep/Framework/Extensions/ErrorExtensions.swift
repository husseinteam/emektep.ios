//
//  ErrorExtensions.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 29.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit
import Foundation

extension NSError {
    
    func log() {
        print(self.description)
    }
    
}
//
//  EnumerationExtensions.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 24.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation

extension Array {
    
    func all(body: (Array.Generator.Element) -> Bool) -> Bool {
        
        for el in self {
            if !body(el) {
                return false;
            }
        }
        return true;
        
    }
    
}
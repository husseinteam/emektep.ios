//
//  File.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 25.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation

infix operator ~> { associativity left precedence 140 }
infix operator ~ { associativity left precedence 140 }
infix operator %% { associativity left precedence 140 }

func ~(delaySeconds: Double, callback: () -> Void) {

    let delay = delaySeconds * Double(NSEC_PER_SEC)
    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
    dispatch_after(time, dispatch_get_main_queue(), callback)
    
}

func %%(number: Int, mod: Int) -> Int {
    return (number % mod + mod) % mod    
}

func ~> <TElement>(container: [TElement], syncback: (TElement, last: Bool, done: () -> Void) -> Void) -> Void {

    let element = container.first
    guard element != nil else {
        return
    }
    let queue = NSOperationQueue()
    var ops = [NSBlockOperation]()
    for (i, element) in container.enumerate() {
        if i == 0 {
            ops.append(NSBlockOperation {
                let dispatchGroup = dispatch_group_create()
                dispatch_group_enter(dispatchGroup)
                syncback(element, last: i == container.count - 1, done: {
                    dispatch_group_leave(dispatchGroup)
                })
                // wait until anAsyncMethod is completed
                dispatch_group_wait(dispatchGroup, DISPATCH_TIME_FOREVER)
            })
        } else {
            let nextop = NSBlockOperation {
                let dispatchGroup = dispatch_group_create()
                dispatch_group_enter(dispatchGroup)
                syncback(element, last: i == container.count - 1, done: {
                    dispatch_group_leave(dispatchGroup)
                })
                // wait until anAsyncMethod is completed
                dispatch_group_wait(dispatchGroup, DISPATCH_TIME_FOREVER)
            }
            nextop.addDependency(ops.last!)
            ops.append(nextop)
        }
    }
    queue.addOperations(ops, waitUntilFinished: false)
    
}


//
//  UIExtensions.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 28.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import UIKit

extension UIScreen {
    var centerX: CGFloat {
        return bounds.width * 0.5
    }
    var centerY: CGFloat {
        return bounds.height * 0.5
    }
}

extension CGRect {
    
    mutating func scaleToCenter(scaleX: CGFloat, scaleY: CGFloat) {
        
        self = CGRectApplyAffineTransform(self, CGAffineTransformMakeScale(scaleX, scaleY))
    
    }
    
}

extension UIView {
    
    func loopThrough<V: UIView>(inout list: [V]) {
        for view in self.subviews as [UIView] {
            if view is V {
                list.append(view as! V);
            } else {
                view.loopThrough(&list);
            }
        }
    }
    
    func putOnCenter(scaleX: CGFloat, scaleY: CGFloat) {
        
        self.bounds.scaleToCenter(scaleX, scaleY: scaleY)
        self.center.x = UIScreen.mainScreen().centerX
        self.center.y = UIScreen.mainScreen().centerY
        
    }
    
    func putOnCenterOfSuperview(view: UIView, scaled by: CGFloat = 1.0, dx: CGFloat = 0, dy: CGFloat = 0) {
        
        let cn = NSLayoutConstraint(item: self, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: self.frame.width * by)
        let cm = NSLayoutConstraint(item: self, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: self.frame.height * by)
        let cx = NSLayoutConstraint(item: self, attribute: .CenterX, relatedBy: .Equal, toItem: view, attribute: .CenterX, multiplier: 1, constant: dx * by)
        let cy = NSLayoutConstraint(item: self, attribute: .CenterY, relatedBy: .Equal, toItem: view, attribute: .CenterY, multiplier: 1, constant: dy * by)
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activateConstraints([cn, cm, cx, cy])
        
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.nextResponder()
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

extension UIStoryboardSegue {
    
    var contentViewController: UIViewController {
        get {
            return self.destinationViewController is UINavigationController ? (self.destinationViewController as! UINavigationController).topViewController! : self.destinationViewController
        }
    }
    
}

extension UIColor {
    
    convenience init(hash: String, alpha: CGFloat = 1.0) {
        let rash = hash.stringByReplacingOccurrencesOfString("#", withString: "")
        let segments = [rash[0, 2], rash[2, 4], rash[4, 6]]
        let colors = segments.map { $0.allChars { c in c == "0" } ? 0.0 : CGFloat(Int($0, radix: 16) ?? 0) / 255 }
        self.init(red: colors[0], green: colors[1], blue: colors[2], alpha: alpha)
    }
    
    func convertToImage(size: CGSize) -> UIImage {
        let rect = CGRectMake(0, 0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
}

extension UITableView {
    
    func scrollToBottom() {
        let y = self.contentSize.height - self.bounds.size.height
        if y > -self.contentInset.top {
            100 ~ { self.setContentOffset(CGPointMake(0, y), animated: true) }
        }
    }

}

extension CGSize {
    mutating func scaled(by measure: CGFloat) {
        self.width *= measure
        self.height *= measure
    }
    func scale(by measure: CGFloat) -> CGSize {
        var newsize = self
        newsize.width *= measure
        newsize.height *= measure
        return newsize
    }
}

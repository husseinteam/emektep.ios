//
//  AnimationExtensions.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 23.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation
import UIKit
import Canvas

public protocol Fadeable {
    var alpha: CGFloat {get set}
    
    mutating func fadeIn(duration: NSTimeInterval, delay: NSTimeInterval, completion: (Bool) -> Void)
    mutating func fadeOut(duration: NSTimeInterval, delay: NSTimeInterval, completion: (Bool) -> Void)
}

public extension Fadeable {
    public mutating func fadeIn(duration: NSTimeInterval = 0.5, delay: NSTimeInterval = 0.0, completion: ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animateWithDuration(duration, delay: delay, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.alpha = 1.0
            }, completion: completion)  }
    
    public mutating func fadeOut(duration: NSTimeInterval = 0.5, delay: NSTimeInterval = 0.0, completion: (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animateWithDuration(duration, delay: delay, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.alpha = 0.0
            }, completion: completion)
    }
    public mutating func fade(to alpha: CGFloat, forDuration: NSTimeInterval, completion: ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        self.alpha = alpha < 0 ? 1.0 : 0.0
        UIView.animateWithDuration(forDuration, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.alpha = abs(alpha)
            }, completion: completion)
    }
}

extension UIView: Fadeable {}


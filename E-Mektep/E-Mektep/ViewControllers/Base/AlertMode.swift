//
//  AlertMode.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 24.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import Foundation

enum AlertMode {
    case Error
    case Notice
    case Warning
    case Info
    case Success
}
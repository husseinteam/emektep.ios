//
//  MembershipViewController.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 22.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit
import SCLAlertView

class PublicViewController: UIViewController {

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.setNavigationBarHidden(true
            , animated: false);
    }
    
}

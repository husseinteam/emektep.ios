//
//  GrandViewController.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 22.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON
import EasyTipView
import PKHUD
import ReachabilitySwift

class GrandViewController: UIViewController {
  
    override func viewDidAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false
            , animated: true)
        super.viewDidAppear(animated)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent;
    }
    
    private var tipView: EasyTipView?;
    func showTooltip(message: String, forView: UIView) {
        
        var preferences = EasyTipView.Preferences();
        preferences.drawing.font = UIFont(name: "Helvetica Neue", size: 13)!;
        preferences.drawing.foregroundColor = UIColor.whiteColor();
        preferences.drawing.backgroundColor = UIColor.orangeColor();
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.Bottom;
        self.tipView?.dismiss();
        self.tipView = EasyTipView(text: message, preferences: preferences)
        self.tipView?.show(forView: forView);
        
    }
    
    func dismissTooltip() {
        self.tipView?.dismiss();
    }
    
}

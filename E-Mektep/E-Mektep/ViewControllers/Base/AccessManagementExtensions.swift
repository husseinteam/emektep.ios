//
//  GrandViewController.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 22.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON
import EasyTipView
import PKHUD
import ReachabilitySwift
import SCLAlertView

extension UIViewController {
    
    func navigateBack(afterNavigate: ((UIViewController?) -> Void)? = nil) {
        if let _ = self.navigationController?.popViewControllerAnimated(true) {
            afterNavigate?(self.navigationController?.topViewController)
        }
    }
    
    func jumpToRoot() {
        if let nav = self.storyboard?.instantiateViewControllerWithIdentifier("RootNavigator") as? UINavigationController {
            self.presentViewController(nav, animated: true, completion: {
                
            })
        }
    }
    
    func showAlert(mode: AlertMode, title: String, subTitle: String, doneback: (()-> Void)? = nil) {
        
        let alertView = SCLAlertView();
        alertView.showCloseButton = false;
        alertView.addButton("Tamam") {
            doneback?();
        }
        switch mode {
        case .Error:
            alertView.showError(title, subTitle: subTitle);
            break;
        case .Notice:
            alertView.showNotice(title, subTitle: subTitle);
            break;
        case .Warning:
            alertView.showWarning(title, subTitle: subTitle);
            break;
        case .Info:
            alertView.showInfo(title, subTitle: subTitle);
            break;
        case .Success:
            alertView.showSuccess(title, subTitle: subTitle);
            break;
        }
    }
    
    func grantAccess(to route: String, granted: (data: JSON) -> Void, showHud: Bool = true, denied: ((msg: String, done: () -> Void) -> Void)? = nil) {
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return
        }
        reachability.whenReachable = { reachability in
            dispatch_async(dispatch_get_main_queue()) {
                var parameters = [String:String]();
                if let token = AppData.Session.Token {
                    parameters["token"] = token;
                    if showHud {
                        HUD.show(.LabeledProgress(title: "E-Mektep", subtitle: "Yükleniyor.."))
                    }
                    Alamofire.request(.POST, AppData.URL.toApi(route),
                        parameters: parameters, encoding: .JSON)
                        .responseJSON(completionHandler: { response in
                            if showHud {
                                HUD.hide(animated: false, completion: nil)
                            }
                            if let jsonString = response.result.value {
                                let json = JSON(jsonString);
                                if json["done"].boolValue {
                                    granted(data: json["data"])
                                } else {
                                    denied?(msg: json["messages"][0].stringValue) { self.jumpToRoot() }
                                }
                            }
                        });
                } else if let tckn = AppData.UserDefaults.TCKN {
                    if showHud {
                        HUD.show(.LabeledProgress(title: "E-Mektep", subtitle: "Yükleniyor.."))
                    }
                    parameters["Logon"] = tckn;
                    parameters["Password"] = AppData.UserDefaults.Password;
                    Alamofire.request(.POST, AppData.URL.toApi(route),
                        parameters: parameters, encoding: .JSON)
                        .responseJSON(completionHandler: { response in
                            if showHud {
                                HUD.hide(animated: false, completion: nil)
                            }
                            if let jsonString = response.result.value {
                                let json = JSON(jsonString);
                                if json["done"].boolValue {
                                    AppData.Session.Token = json["token"].stringValue;
                                    self.grantAccess(to: route, granted: granted, denied: denied);
                                } else {
                                    denied?(msg: json["messages"][0].stringValue) { self.jumpToRoot() }
                                }
                            }
                        })
                } else {
                    denied?(msg: "Geçersiz Kimlik! Yeniden Giriş Yapınız") { self.jumpToRoot() }
                }
            }
        }
        reachability.whenUnreachable = { reachability in
            dispatch_async(dispatch_get_main_queue()) {
                HUD.show(.LabeledError(title: "E-Mektep", subtitle: "Internet Bağlantınızı Kontrol Ediniz"));
                HUD.hide(afterDelay: 1.7, completion: { (fine) in
                    exit(0)
                })
            }
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
}
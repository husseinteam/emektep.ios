//
//  MainScreenViewController.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 2.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit

class MainScreenViewController: PublicViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCollectionView()
    }
    
    func setupCollectionView() {
        self.collectionView.registerNib(UINib(nibName: "MainScreenCell", bundle: nil), forCellWithReuseIdentifier: "MainScreenCell")
        self.loadModel()
    }
    
    func loadModel() {
        let edge = AppData.uiSize.width / 3.0 - 0.1
        self.model = [
            MainScreenModel(color: .Green, title: "Yeşili Koru", width: edge, height: edge),
            MainScreenModel(color: .Yellow, title: "Haftalık Ders Çizelgesi", width: edge, height: edge)
            .setImageName("logo-schedule")
            .setCallback({ (model) in
                self.performSegueWithIdentifier("classSchedule", sender: self)
            }),
            MainScreenModel(color: .Red, title: "Ateşe Düşme", width: edge, height: edge),
            MainScreenModel(color: .Blue, title: "Gökyüzünü Özle", width: edge, height: edge * 2),
            MainScreenModel(color: .Brown, title: "Verilen Dersler", width: edge * 2, height: edge * 2)
            .setImageName("logo-lecture")
            .setCallback({ (model) in
                self.performSegueWithIdentifier("lectures", sender: self)
            }),
            MainScreenModel(color: .Orange, title: "Yönetim Masası", width: edge * 2, height: edge)
                .setImageName("logo-dashboard")
                .setCallback({ (model) in
                    self.performSegueWithIdentifier("dashboard", sender: self)
                }),
            MainScreenModel(color: .Magenta, title: "Magenta", width: edge, height: edge)
        ]
    }
    
    var model: [MainScreenModel]! {
        didSet {
            self.collectionView.reloadData()
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let id = segue.identifier {
            switch id {
            case "classSchedule":
                break
            case "lectures":
                break
            case "dashboard":
                if let tab = segue.contentViewController as? UITabBarController {
                    if let dashboard = (tab.selectedViewController as! UINavigationController)
                        .topViewController as? ActivityViewController {
                        dashboard.setExam(1)
                    }
                }
                break
            default: break
            }
        }
    }
    

}

extension MainScreenViewController {
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        //  self.loadModel()
    }
}

extension MainScreenViewController : UICollectionViewDataSource {
    
    //1
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.model.count
    }
    
    //3
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("MainScreenCell", forIndexPath: indexPath) as? MainScreenCell {
            cell.model = self.model[indexPath.item]
            return cell
        }
        // Configure the cell
        return UICollectionViewCell()
    }
    
}

extension MainScreenViewController : UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.model[indexPath.item].callback?(model: self.model[indexPath.item])
    }
    
}

extension MainScreenViewController : UICollectionViewDelegateFlowLayout  {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return self.model[indexPath.item].size
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
}


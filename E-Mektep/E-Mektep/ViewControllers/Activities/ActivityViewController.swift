/*
Copyright (c) 2015, Apple Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3.  Neither the name of the copyright holder(s) nor the names of any contributors
may be used to endorse or promote products derived from this software without
specific prior written permission. No license is granted to the trademarks of
the copyright holders even if such marks are included in this software.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import UIKit
import ResearchKit
import PKHUD

enum Activity: Int {
    case Survey, Microphone, Tapping
    
    static var allValues: [Activity] {
        return [self.init(rawValue: 0)!, self.init(rawValue: 1)!, self.init(rawValue: 2)!]
    }
    
    var title: String {
        switch self {
            case .Survey:
                return "Survey"
            case .Microphone:
                return "Microphone"
            case .Tapping:
                return "Tapping"
        }
    }
    
    var subtitle: String {
        switch self {
            case .Survey:
                return "Answer 6 short questions"
            case .Microphone:
                return "Voice evaluation"
            case .Tapping:
                return "Test tapping speed"
        }
    }
}

class ActivityViewController: UITableViewController {
    
    @IBAction func BackTapped(sender: UIBarButtonItem) {
        self.jumpToRoot()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false
            , animated: false);
    }
    
    private var steps: [ORKStep] = [ORKStep]()
    
    // MARK: UITableViewDataSource
    
    private var examID: Int!
    
    func setExam(examID: Int) {
        self.examID = examID
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section == 0 else { return 0 }
        
        return Activity.allValues.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("activityCell", forIndexPath: indexPath)
        
        if let activity = Activity(rawValue: indexPath.row) {
            cell.textLabel?.text = activity.title
            cell.detailTextLabel?.text = activity.subtitle
        }

        return cell
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let activity = Activity(rawValue: indexPath.row) else { return }
        
        switch activity {
            case .Survey:
                self.generateSurveyTask { task in
                    let taskViewController = ORKTaskViewController(task: task, taskRunUUID: NSUUID())
                    taskViewController.delegate = self
                    self.navigationController?.presentViewController(taskViewController, animated: true, completion: nil)
                    return
                }
            case .Microphone:
                let taskViewController = ORKTaskViewController(task: StudyTasks.microphoneTask, taskRunUUID: NSUUID())
                
                do {
                    let defaultFileManager = NSFileManager.defaultManager()
                    
                    // Identify the documents directory.
                    let documentsDirectory = try defaultFileManager.URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: false)
                    
                    // Create a directory based on the `taskRunUUID` to store output from the task.
                    let outputDirectory = documentsDirectory.URLByAppendingPathComponent(taskViewController.taskRunUUID.UUIDString)
                    try defaultFileManager.createDirectoryAtURL(outputDirectory, withIntermediateDirectories: true, attributes: nil)
                    
                    taskViewController.outputDirectory = outputDirectory
                }
                catch let error as NSError {
                    fatalError("The output directory for the task with UUID: \(taskViewController.taskRunUUID.UUIDString) could not be created. Error: \(error.localizedDescription)")
                }
                taskViewController.delegate = self
                self.navigationController?.presentViewController(taskViewController, animated: true, completion: nil)
            case .Tapping:
                let taskViewController = ORKTaskViewController(task: StudyTasks.tappingTask, taskRunUUID: NSUUID())
                taskViewController.delegate = self
                self.navigationController?.presentViewController(taskViewController, animated: true, completion: nil)
        }

    }
}

extension ActivityViewController {
    
    func generateSurveyTask(task: (ORKOrderedTask) -> Void) {
        HUD.show(.LabeledProgress(title: "E-Mektep", subtitle: "Yükleniyor.."))
        super.grantAccess(to: "activity/questionsof/\(self.examID)", granted: { (qdata) in
            var qmodels = [QuestionModel]()
            for qrow in qdata.arrayValue {
                qmodels.append(QuestionModel(json: qrow))
            }
            qmodels ~> { (qmodel, last, done) in
                super.grantAccess(to: "activity/choicesof/\(qmodel.id)", granted: { (tcdata) in
                    qmodel.testChoices = [TestChoiceModel]()
                    for tcrow in tcdata.arrayValue {
                        qmodel.testChoices?.append(TestChoiceModel(json: tcrow))
                    }
                    done()
                    if last {
                        self.steps = qmodels.map { $0.toQuestionStep() }
                        let summaryStep = ORKCompletionStep(identifier: "SummaryStep")
                        summaryStep.title = "Right. Off you go!"
                        summaryStep.text = "That was easy!"
                        self.steps += [summaryStep]
                        task(ORKOrderedTask(identifier: "SurveyTask", steps: self.steps))
                        HUD.hide(animated: true)
                    }
                }, showHud: false) { (msg, done) in
                    HUD.hide(animated: true)
                    super.showAlert(.Error, title: "E-Mektep", subTitle: msg, doneback: done)
                }
            }
        }, showHud: false) { (msg, done) in
            HUD.hide(animated: true)
            super.showAlert(.Error, title: "E-Mektep", subTitle: msg, doneback: done)
        }
    }
}

extension ActivityViewController : ORKTaskViewControllerDelegate {
    
    func taskViewController(taskViewController: ORKTaskViewController, didFinishWithReason reason: ORKTaskViewControllerFinishReason, error: NSError?) {
        let finalResult = taskViewController.result
        for (id, _) in self.steps.map({ ($0.identifier, $0) }) {
            let stepResult = finalResult.stepResultForStepIdentifier(id)
            if let orkResults = stepResult?.results as? [ORKQuestionResult] {
                for orkResult in orkResults {
                    if let choiceResult = orkResult as? ORKChoiceQuestionResult {
                        print(choiceResult.choiceAnswers!)
                    }
                }
            }
        }
        
        taskViewController.dismissViewControllerAnimated(true, completion: nil)
    }

}




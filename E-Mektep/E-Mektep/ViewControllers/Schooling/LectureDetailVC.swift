//
//  LectureDetailVC.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 25.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit

class LectureDetailVC: GrandViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false
            , animated: true);
        self.loadData();
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.setNavigationBarHidden(false
            , animated: false);
        1.0 ~ { self.showTooltip("Dersi Alan Öğrenciler", forView: self.studentNavigatorButton) }
        1.7 ~ { self.dismissTooltip() }
    }
    

    @IBOutlet weak var studentNavigatorButton: UIButton!
    @IBAction func navigateToStudents(sender: UIButton) {
        self.performSegueWithIdentifier("students", sender: self)
    }
    @IBAction func navigateToLectures(sender: UIBarButtonItem) {
        self.navigateBack();
    }
    
    func loadData() {
        dersKoduLabel.text = model.DersKodu;
        dersAdiLabel.text = model.DersAdi;
        teorikSaatLabel.text = String(model.TeorikSaat);
        uygulamaSaatLabel.text = String(model.UygulamaSaat);
        dersKredisiLabel.text = String(model.Kredi);
        ogretimUyesiLabel.text = model.OgretimUyesi;
    }
    
    @IBOutlet weak var dersKoduLabel: UILabel!
    @IBOutlet weak var dersAdiLabel: UILabel!
    @IBOutlet weak var teorikSaatLabel: UILabel!
    @IBOutlet weak var uygulamaSaatLabel: UILabel!
    @IBOutlet weak var dersKredisiLabel: UILabel!
    @IBOutlet weak var ogretimUyesiLabel: UILabel!
    
    
    var model: LectureCellModel!

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let id = segue.identifier {
            switch id {
            case "students":
                if let ogrencivc = segue.destinationViewController as? StudentVC {
                    ogrencivc.DersID = model.DersID;
                }
                
            default: break;
            }
        }
    }
    

}

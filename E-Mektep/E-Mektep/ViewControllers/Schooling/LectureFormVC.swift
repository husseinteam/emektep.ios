//
//  LectureFormVC.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 25.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit

class LectureFormVC: GrandViewController {

    @IBOutlet weak var dersKoduAF: AnimeField!
    @IBOutlet weak var dersAdiAF: AnimeField!
    @IBOutlet weak var teorikSaatAF: AnimeField!
    @IBOutlet weak var uygulamaliSaat: AnimeField!
    @IBOutlet weak var dersKredisiAF: AnimeField!
    @IBOutlet weak var form: AnimableForm!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setAF();
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated);
        self.navigationItem.title = self.model != nil ? "Dersi Güncelle" : "Ders Ekle";
        self.updateUI(self.model);
    }

    @IBAction func kaydetTapped(sender: UIButton) {
        
        if self.model != nil {
            self.updateModel()
        } else {
            self.insertModel()
        }
    }
    
    private var inserted: Bool! = false
    private func insertModel() {
        self.form.submit("tedrisat/ekle/ders", postback: { (data, token) in
            self.updateUI(LectureCellModel(json: data));
            self.inserted = true
            self.showAlert(.Success, title: "E-Mektep",
                subTitle: "Ekleme Başarılı")
        });
    }
    
    private func updateModel() {
        self.form.submit("tedrisat/duzenle/ders/\(self.model.DersID)", postback: { (data, token) in
            self.updateUI(LectureCellModel(json: data));
            self.showAlert(.Success, title: "E-Mektep",
                subTitle: "Güncelleme Başarılı")
        });
    }
    
    private func setAF() {
        self.dersKoduAF.setContentType(.Text(max: 15));
        self.dersAdiAF.setContentType(.Text(max: 128));
        self.teorikSaatAF.setContentType(.Pickered(range: [Int](0...5).map { (id: $0, value: String($0)) }));
        self.uygulamaliSaat.setContentType(.Pickered(range: [Int](0...5).map { (id: $0, value: String($0)) }));
        self.dersKredisiAF.setContentType(.Pickered(range: [Int](0...5).map { (id: $0, value: String($0)) }))
    }
    
    private func updateUI(m: LectureCellModel!) {
        if m != nil {
            self.dersKoduAF.value = m.DersKodu;
            self.dersAdiAF.value = m.DersAdi;
            self.teorikSaatAF.value = m.TeorikSaat.astr();
            self.uygulamaliSaat.value = m.UygulamaSaat.astr();
            self.dersKredisiAF.value = m.DersKredisi.astr();
            self.model = m;
        }
    }
    
    var model: LectureCellModel!
    
    @IBAction func goBack(sender: UIBarButtonItem) {
        self.navigateBack { vc in
            if let dersvc = vc as? LectureVC {
                dersvc.updateSelectedModel(self.model, inserted: self.inserted)
            }
        };
    }

}

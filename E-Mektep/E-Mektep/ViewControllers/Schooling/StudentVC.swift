//
//  LectureTVC.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 24.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class StudentVC: GrandViewController {
    
    @IBOutlet var studentsTableView: UITableView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        super.grantAccess(to: "tedrisat/ogrenciler/\(self.DersID)", granted: { (data) in
            self.prepareTableView(data)
            self.prepareSelf()
            }) { (msg, done) in
                super.showAlert(.Error, title: "E-Mektep", subTitle: msg, doneback: done)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.setNavigationBarHidden(false
            , animated: false);
    }
    
    @IBAction func AddStudentToLecture(sender: UIBarButtonItem) {
        
        self.performSegueWithIdentifier("studentAddForm", sender: self)
        
    }
    
    
    @IBAction func GoBack(sender: UIBarButtonItem) {
        
        self.navigateBack();
        
    }
    private var model = [StudentCellModel]();
    private var selectedIndex: NSIndexPath? = nil
    private var selectedModel: StudentCellModel!
    func updateSelectedModel(newModel: StudentCellModel!, inserted: Bool) {
        
        if newModel != nil {
            if inserted {
                self.model.append(newModel);
                self.studentsTableView.reloadData()
                self.studentsTableView.scrollToBottom()
            } else if let index = self.selectedIndex {
                self.model[index.row] = newModel;
                self.studentsTableView.reloadRowsAtIndexPaths([index], withRowAnimation: .Automatic)
            }
        }
        
    }
    var DersID: Int!
    
    private func prepareTableView(data: JSON) {
        self.studentsTableView.registerNib(UINib(nibName: "StudentCell", bundle: nil), forCellReuseIdentifier: "Cell");
        for row in data.arrayValue {
            model.append(StudentCellModel(json: row));
        }
        self.studentsTableView.reloadData();
    }
    private func prepareSelf() {
        //self.studentsTableView.scrollToBottom()
    }
    
}

extension StudentVC: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if model.count > 0 {
            if let studentCell = self.studentsTableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as? StudentCell {
                studentCell.model = model[indexPath.row];
                studentCell.selectionStyle = .None;
                return studentCell;
            }
        }
        return UITableViewCell();
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .Normal, title: "Sil") { action, index in
            self.model.removeAtIndex(index.row);
            tableView.deleteRowsAtIndexPaths([index], withRowAnimation: .Fade)
        }
        delete.backgroundColor = UIColor(red: 219/255, green: 40/255, blue: 40/255, alpha: 1.0)
        
        let edit = UITableViewRowAction(style: .Normal, title: "Düzenle") { action, index in
            self.selectedModel = self.model[index.row];
            self.selectedIndex = index
            self.performSegueWithIdentifier("studentEditForm", sender: self);
        }
        edit.backgroundColor = UIColor(red: 0.0, green: 130/255, blue: 124/255, alpha: 1.0)
        
        return [delete, edit]
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false;
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true;
    }
}

extension StudentVC : UITableViewDelegate {

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedModel = self.model[indexPath.row];
        self.performSegueWithIdentifier("studentDetails", sender: self);
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let id = segue.identifier {
            switch id {
            case "studentDetails":
                if let ogrencivc = segue.destinationViewController as? StudentDetailVC {
                    ogrencivc.model = self.selectedModel;
                }
            case "studentEditForm":
                self.studentsTableView.endEditing(true)
                if let dersvc = segue.destinationViewController as? StudentFormVC {
                    dersvc.model = self.selectedModel;
                }
                
            case "studentAddForm":
                break;
                
            default: break;
            }
        }
    }
}



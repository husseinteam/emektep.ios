//
//  LectureTVC.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 24.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class LectureVC: GrandViewController {
    
    @IBOutlet var lecturesTableView: UITableView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        super.grantAccess(to: "tedrisat/dersler", granted: { (data) in
            self.prepareTableView(data);
            self.prepareSelf(); 
            }) { (msg, done) in
                super.showAlert(.Error, title: "E-Mektep", subTitle: msg, doneback: done)
        }
    }

    @IBAction func addLecture(sender: UIBarButtonItem) {
        self.performSegueWithIdentifier("lectureAddForm", sender: self)
    }
    @IBAction func Logout(sender: UIBarButtonItem) {
        
        AppData.Session.Token = nil;
        self.navigateBack();
        
    }
    private var model = [LectureCellModel]();
    private var selectedIndex: NSIndexPath? = nil
    private var selectedModel: LectureCellModel!
    func updateSelectedModel(newModel: LectureCellModel!, inserted: Bool) {
        
        if newModel != nil {
            if inserted {
                self.model.append(newModel);
                self.lecturesTableView.reloadData()
                self.lecturesTableView.scrollToBottom()
            } else if let index = self.selectedIndex {
                self.model[index.row] = newModel;
                self.lecturesTableView.reloadRowsAtIndexPaths([index], withRowAnimation: .Automatic)
            }
        }
        
    }
    
    private func prepareTableView(data: JSON) {
        self.lecturesTableView.registerNib(UINib(nibName: "LectureCell", bundle: nil), forCellReuseIdentifier: "Cell");
        for row in data.arrayValue {
            model.append(LectureCellModel(json: row));
        }
        self.lecturesTableView.reloadData();
    }
    private func prepareSelf() {
        //self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
}

extension LectureVC: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if model.count > 0 {
            if let lectureCell = self.lecturesTableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as? LectureCell {
                lectureCell.model = model[indexPath.row];
                lectureCell.selectionStyle = .None;
                return lectureCell;
            }
        }
        return UITableViewCell();
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        let movedObject = self.model[sourceIndexPath.row]
        self.model.removeAtIndex(sourceIndexPath.row)
        self.model.insert(movedObject, atIndex: destinationIndexPath.row)
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false;
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true;
    }
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .Normal, title: "Sil") { action, index in
            self.model.removeAtIndex(index.row);
            tableView.deleteRowsAtIndexPaths([index], withRowAnimation: .Fade)
        }
        delete.backgroundColor = UIColor(red: 219/255, green: 40/255, blue: 40/255, alpha: 1.0)
        
        let edit = UITableViewRowAction(style: .Normal, title: "Düzenle") { action, index in
            self.selectedModel = self.model[index.row];
            self.selectedIndex = index
            self.performSegueWithIdentifier("lectureEditForm", sender: self);
        }
        edit.backgroundColor = UIColor(red: 0.0, green: 130/255, blue: 124/255, alpha: 1.0)
        
        return [delete, edit]
    }

}

extension LectureVC : UITableViewDelegate {

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedModel = self.model[indexPath.row];
        self.selectedIndex = indexPath;
        self.performSegueWithIdentifier("lectureDetails", sender: self);
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let id = segue.identifier {
            switch id {
            case "lectureDetails":
                if let dersvc = segue.destinationViewController as? LectureDetailVC {
                    dersvc.model = self.selectedModel;
                }
            case "lectureEditForm":
                self.lecturesTableView.endEditing(true)
                if let dersvc = segue.destinationViewController as? LectureFormVC {
                    dersvc.model = self.selectedModel;
                }
                
            case "lectureAddForm":
                break;
                
            default: break;
            }
        }
    }
}



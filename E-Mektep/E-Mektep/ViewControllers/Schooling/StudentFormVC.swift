//
//  StudentFormVC.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 25.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit

class StudentFormVC: GrandViewController {

    @IBOutlet weak var form: AnimableForm!
    @IBOutlet weak var adSoyadAF: AnimeField!
    @IBOutlet weak var ogrenciNoAF: AnimeField!
    @IBOutlet weak var kayitYiliAF: AnimeField!
    @IBOutlet weak var kayitTipiAF: AnimeField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setAF();
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated);
        self.navigationItem.title = self.model != nil ? "Öğrenci Güncelle" : "Öğrenci Ekle";
        self.updateUI(self.model);
    }

    @IBAction func kaydetTapped(sender: UIButton) {
        
        if self.model != nil {
            self.updateModel()
        } else {
            self.insertModel()
        }
    }
    
    private var inserted: Bool! = false
    private func insertModel() {
        self.form.submit("tedrisat/ekle/ogrenci", postback: { (data, token) in
            self.updateUI(StudentCellModel(json: data));
            self.inserted = true
            self.showAlert(.Success, title: "E-Mektep",
                subTitle: "Ekleme Başarılı")
        });
    }
    
    private func updateModel() {
        self.form.submit("tedrisat/duzenle/ogrenci/\(self.model.UyeID)", postback: { (data, token) in
            self.updateUI(StudentCellModel(json: data));
            self.showAlert(.Success, title: "E-Mektep",
                subTitle: "Güncelleme Başarılı")
        });
    }
    
    private func setAF() {
        self.adSoyadAF.setContentType(.Text(max: 15));
        self.kayitTipiAF.setContentType(.Pickered(range: RegisterType.getRange()));
        self.kayitYiliAF.setContentType(.Pickered(range: AppData.CurrentDate.populateSince(1980).map { (id: $0, value: String($0)) } ))
        self.ogrenciNoAF.setContentType(.Text(max: 128));
    }
    
    private func updateUI(m: StudentCellModel!) {
        if m != nil {
            self.adSoyadAF.value = m.AdiSoyadi;
            self.kayitTipiAF.value = m.KayitTipi.description().text;
            self.kayitTipiAF.pickedID = m.KayitTipi.description().id;
            self.kayitYiliAF.value = m.KayitYili.astr();
            self.ogrenciNoAF.value = m.OgrenciNo
            self.model = m;
        }
    }
    
    var model: StudentCellModel!
    
    @IBAction func goBack(sender: UIBarButtonItem) {
        self.navigateBack { vc in
            if let ogrvc = vc as? StudentVC {
                ogrvc.updateSelectedModel(self.model, inserted: self.inserted)
            }
        };
    }

}

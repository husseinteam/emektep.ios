//
//  LectureDetailVC.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 25.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit

class StudentDetailVC: GrandViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false
            , animated: true);
        self.loadData();
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.setNavigationBarHidden(false
            , animated: false);
    }
    
    @IBAction func navigateToLectures(sender: UIBarButtonItem) {
        self.navigateBack();
    }
    
    func loadData() {
        ogrenciNoLabel.text = model.OgrenciNo;
        ogrenciAdiSoyadiLabel.text = model.AdiSoyadi;
        kayitYiliLabel.text = String(model.KayitYili);
        kayitTipiLabel.text = model.KayitTipi.description().text;
    }
    
    @IBOutlet weak var ogrenciNoLabel: UILabel!
    @IBOutlet weak var ogrenciAdiSoyadiLabel: UILabel!
    @IBOutlet weak var kayitYiliLabel: UILabel!
    @IBOutlet weak var kayitTipiLabel: UILabel!

    
    
    var model: StudentCellModel!

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

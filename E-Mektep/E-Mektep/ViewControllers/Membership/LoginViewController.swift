//
//  LoginViewController.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 22.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit
import Canvas

class LoginViewController: PublicViewController {
    
    @IBOutlet weak var tckn: AnimeField!
    @IBOutlet weak var password: AnimeField!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginSegment: AnimableForm!
    
    @IBOutlet weak var logo: UIImageView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.navigationController?.setNavigationBarHidden(true
            , animated: false);
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tckn.value = AppData.UserDefaults.TCKN
        self.password.value = AppData.debugModeOn ? "1q2w3e4r5t" : self.password.value
        self.tckn.setContentType(.TCKN(max: 11))
        self.password.setContentType(.Password(max: 10)).setValidator { (field) -> Bool in
            return field.value.characters.count >= 3
        }
    }
    
    @IBAction func loginTapped(sender: UIButton) {
        
        loginSegment.submit("ms/login", postback: { (data, token) in
            AppData.Session.Token = token
            AppData.UserDefaults.TCKN = self.tckn.value
            AppData.UserDefaults.Password = self.password.value
            self.performSegueWithIdentifier("mainScreen", sender: self)
        })
        
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        if toInterfaceOrientation == .LandscapeLeft || toInterfaceOrientation == .LandscapeRight {
            self.logo.hidden = true;
        } else  {
            self.logo.hidden = false;
        }
    }
}

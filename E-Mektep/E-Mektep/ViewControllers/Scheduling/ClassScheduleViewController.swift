//
//  ClassScheduleViewController.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 30.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit
import SwiftyJSON

class ClassScheduleViewController: GrandViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
//        super.grantAccess(to: "cizelge/dersler", granted: { (data) in
//            self.prepareTableView(data);
//        }) { (msg, done) in
//            super.showAlert(.Error, title: "E-Mektep", subTitle: msg, doneback: done)
//        }
    }
    
    @IBAction func GoBack(sender: UIBarButtonItem) {
        self.navigateBack()
    }
    
    @IBAction func addClass(sender: UIBarButtonItem) {
        //self.performSegueWithIdentifier("lectureAddForm", sender: self)
    }
    
    @IBAction func daySegmentValueChanged(sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            self.setDay(.Pazartesi)
        case 1:
            self.setDay(.Sali)
        case 2:
            self.setDay(.Carsamba)
        case 3:
            self.setDay(.Persembe)
        case 4:
            self.setDay(.Cuma)
        case 5:
            self.setDay(AppData.CurrentDate.dayOfWeek())
        case 6:
            self.setDay(.HaftaIci)
        default:
            fatalError()
        }
        
    }
    
    @IBOutlet weak var daySegments: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    private var model = [ClassModel]();
    private var filteredModel = [ClassModel]();
    private var selectedIndex: NSIndexPath? = nil
    private var selectedModel: ClassModel!
    
    func setup() {
        self.model = ClassModel.MockedData
        self.filteredModel = self.model
        self.tableView.registerNib(UINib(nibName: "ClassTableCell", bundle: nil), forCellReuseIdentifier: "ClassTableCell");
        self.tableView.reloadData();
    }
    
    private func setDay(day: DayOfWeek) {
        if day == .HaftaIci {
            self.filteredModel = self.model
        } else {
            self.filteredModel = self.model.filter { $0.dayOfWeek == day }
        }
        self.tableView.fadeOut() { done in
            self.tableView.reloadData()
            self.tableView.fadeIn()
        }
        
    }
    
    private func prepareTableView(data: JSON) {
        self.tableView.registerNib(UINib(nibName: "ClassTableCell", bundle: nil), forCellReuseIdentifier: "ClassTableCell");
        for row in data.arrayValue {
            model.append(ClassModel(json: row));
        }
        self.filteredModel = self.model
        self.tableView.reloadData();
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let id = segue.identifier {
            switch id {
            case "classDetails":
                break
            case "classEditForm":
                self.tableView.endEditing(true)
                break
            case "classAddForm":
                break;
            default: break;
            }
        }
    }

}

extension ClassScheduleViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredModel.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if self.filteredModel.count > 0 {
            if let cell = self.tableView.dequeueReusableCellWithIdentifier("ClassTableCell", forIndexPath: indexPath) as? ClassTableCell {
                cell.model = self.filteredModel[indexPath.row];
                cell.selectionStyle = .None;
                return cell;
            }
        }
        return UITableViewCell();
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        let movedObject = self.filteredModel[sourceIndexPath.row]
        self.filteredModel.removeAtIndex(sourceIndexPath.row)
        self.filteredModel.insert(movedObject, atIndex: destinationIndexPath.row)
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false;
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true;
    }
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .Normal, title: "Sil") { action, index in
            self.filteredModel.removeAtIndex(index.row);
            tableView.deleteRowsAtIndexPaths([index], withRowAnimation: .Fade)
        }
        delete.backgroundColor = UIColor(hash: "#DB2828")
        
        let edit = UITableViewRowAction(style: .Normal, title: "Düzenle") { action, index in
            self.selectedModel = self.filteredModel[index.row];
            self.selectedIndex = index
            //self.performSegueWithIdentifier("lectureEditForm", sender: self);
        }
        edit.backgroundColor = UIColor(hash: "#00827C")
        
        return [delete, edit]
    }
    
}

extension ClassScheduleViewController : UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedModel = self.filteredModel[indexPath.row];
        self.selectedIndex = indexPath;
        //self.performSegueWithIdentifier("lectureDetails", sender: self);
    }
}

//
//  ScheduleDateView.swift
//  E-Mektep
//
//  Created by Özgür Sönmez on 1.05.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import UIKit

class ScheduleDateView: CircleView {
    
    func setUp(model: ClassModel) {
        let height = self.frame.height / 5
        let begin = UILabel(frame: CGRectMake(0, self.center.y - height, self.frame.width, height))
        let end = UILabel(frame: CGRectMake(0, self.center.y, self.frame.width, height))
        begin.text = model.beginHour
        end.text = model.endHour
        begin.textColor = model.clockColor.inverted()
        end.textColor = model.clockColor.inverted()
        begin.font = UIFont(name: "OpenSans", size: 9.0)
        end.font = UIFont(name: "OpenSans", size: 9.0)
        begin.textAlignment = .Center
        end.textAlignment = .Center
        begin.center.x = self.center.x
        end.center.x = self.center.x
        self.addSubview(begin)
        self.addSubview(end)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}

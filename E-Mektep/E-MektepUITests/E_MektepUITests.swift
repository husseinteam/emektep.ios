//
//  E_MektepUITests.swift
//  E-MektepUITests
//
//  Created by Özgür Sönmez on 22.04.2016.
//  Copyright © 2016 Özgür Sönmez. All rights reserved.
//

import XCTest

class E_MektepUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        
        let app = XCUIApplication()
        app.secureTextFields["Şifre"].tap()
        app.secureTextFields["Şifre"].typeText("1q2w3e4r5t")
        app.buttons["GİRİŞ"].tap()
        app.buttons["Tamam"].tap()
        
        let dZenleButton = app.tables.cells["Cell"].buttons["Düzenle"]
        
        dZenleButton.swipeLeft()
        dZenleButton.tap()
        
        let dersKredisiTextField = app.textFields["Ders Kredisi"]
        dersKredisiTextField.tap()
        dersKredisiTextField.tap()
        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
}
